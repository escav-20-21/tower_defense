﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [Tooltip("Puntos de salud que queremos para el enemigo")]
    public int healthPoints;

    void Update()
    {
        // Cuando el enemigo se queda sin puntos de salud, se destruye
        if (healthPoints <= 0)
        {
            Destroy(gameObject);
        }
    }
}