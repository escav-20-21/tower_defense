﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDamage : MonoBehaviour
{
    [Tooltip("Daño que causa la bala en los enemigos")]
    public int damage;

    private void OnCollisionEnter(Collision collision)
    {
        // Comprueba si la colisión se produce con un enemigo
        if (collision.gameObject.CompareTag("Enemy"))
        {
            // Quita vida al enemigo
            collision.gameObject.GetComponent<EnemyHealth>().healthPoints -= damage;
        }
        // La bala se destruye en cuanto colisiona con algún objeto
        Destroy(gameObject);
    }
}
