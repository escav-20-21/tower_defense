﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    [Tooltip("Velocidad de la bala")]
    public float bulletSpeed;

    [Tooltip("Referencia al enemigo que se quiere atacar")]
    public Transform enemy;

    void Update()
    {
        // Comprueba si el enemigo existe o ya ha sido destruido
        if (enemy != null)
        {
            // Movimiento de la bala hacia el enemigo según la velocidad especificada
            transform.position = Vector3.MoveTowards(transform.position, enemy.position, bulletSpeed);
        } else
        {
            // Destrucción de la bala, ya que no tiene objetivo que perseguir
            Destroy(gameObject);
        }
    }
}
