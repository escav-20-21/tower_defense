﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    [Tooltip("Posición a la que queremos que se dirija el enemigo")]
    public Vector3 targetPosition;

    void Start()
    {
        // Establece la posición hacia la que se debe mover el enemigo
        GetComponent<NavMeshAgent>().SetDestination(targetPosition);
    }
}
