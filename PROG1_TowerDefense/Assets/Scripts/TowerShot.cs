﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerShot : MonoBehaviour
{
    [Tooltip("Referencia al prefab de la bala")]
    public GameObject bulletPrefab;

    [Tooltip("Referencia al enemigo de la escena al que se quiere disparar")]
    public Transform enemyToShot;

    /// <summary>
    /// Momento del último disparo
    /// </summary>
    public float timeLastShot;

    void Update()
    {
        // Instancia una bala cada vez que el jugador pulsa el espacio
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // Comprueba si el enemigo objetivo existe o ya se ha destruido
            if (enemyToShot != null)
            {
                // Crea la bala en la escena y le asigna el enemigo al que debe atacar
                GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
                bullet.GetComponent<BulletMovement>().enemy = enemyToShot;
                timeLastShot = Time.time;
            }
        }
    }
}
